import { configureStore } from "@reduxjs/toolkit";
import { infoSlice } from "./InfoSlice";

const MainStore = configureStore({
    reducer: {
      info: infoSlice.reducer,
    },
  });

export default MainStore;