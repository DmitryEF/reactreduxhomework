import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export const infoSlice = createSlice({
  name: "info",
  initialState: {
   text: "",
  },
  reducers: {
    setText: (state, action: PayloadAction<string>) => {
      state.text = action.payload;
    },    
  },
});

export const { setText } = infoSlice.actions;

export default infoSlice.reducer;