import { useSelector } from "react-redux";
import { Container, Nav, Navbar } from 'react-bootstrap';
import {
    Link
  } from "react-router-dom";

export default function BootstrapNavigation() {

    const infoText = useSelector((x : any) => x.info.text);

    return (
        <>
            <Navbar expand="lg" className="bg-body-tertiary">
                <Container>
                    <Navbar.Brand href="/home">React-Homework</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link>
                                <Link to={'/home'}>Главная</Link>
                            </Nav.Link>
                            <Nav.Link>
                                <Link to={'/login'}>Авторизация</Link>
                            </Nav.Link>
                            <Nav.Link>
                                <Link to={'/register'}>Регистрация</Link>     
                            </Nav.Link>           
                        </Nav>
                    </Navbar.Collapse>
                    <Navbar.Text>Привет {infoText}</Navbar.Text>
                </Container>
            </Navbar>
        </>
    );
}