import {
    Route,
    Routes,
  } from "react-router-dom";

import HomePage from "../Pages/Home/HomePage";
import LoginPage from "../Pages/Login/LoginPage";
import RegistrationPage from "../Pages/Register/RegistrationPage";
import Page404 from "../Pages/404/Page404";

export default function RoutesInitializer(){
    return(
        <>
        <Routes>
        <Route index element={<HomePage />}/>
        <Route path="home" element={<HomePage />} />
        <Route path="login" element={<LoginPage />} />
        <Route path="register" element={<RegistrationPage />} />
        <Route path="*" element={<Page404 />} />
      </Routes>
        </>
    );
}