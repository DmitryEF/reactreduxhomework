import "bootstrap/dist/css/bootstrap.min.css"

import {
  BrowserRouter
} from "react-router-dom";

import RoutesInitializer from './Routes/RoutesInitializer';
import BootstrapNavigation from './Navigation/BootstrapNavigation';

function App() {
  return (
    <BrowserRouter>
      <BootstrapNavigation />
      <RoutesInitializer />      
    </BrowserRouter>    
  );
}

export default App;
