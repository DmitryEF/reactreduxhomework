import '../Login/AuthorizationForm.css';
import { Button } from "react-bootstrap";
import TdLabeledInput from "../Login/TdLabeledInput";

const RegistrationPage = () => {
    const registrationHandler = () => {  
        const e = document.getElementById("login") as HTMLInputElement;

        if (e.value === ""){
            alert("Введите логин!");
        }
        else{
            alert("Пользователь " + e.value + " зарегистрирован!");
        }
    }

    return(
        <div>
            <div className="Auth-form-container">
            <center>
                <h1 className='LoginForm-h1'>Регистрация в системе</h1>
                <br />
                <table className='LoginForm-table'>
                    <tr className='LoginForm-tr-special'></tr>
                    <tr>
                        <TdLabeledInput componentId="login" placeholderText='введите логин' labelText='Логин:' type='text'/>
                        <td className='LoginForm-td-special'></td>
                    </tr>
                    <tr>
                        <TdLabeledInput componentId="password" placeholderText='введите пароль' labelText='Пароль:' type='password'/>
                        <td className='LoginForm-td-special'></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <Button onClick={registrationHandler}>Зарегистрироваться</Button>
                        </td>
                        <td className='LoginForm-td-special'></td>
                    </tr>
                    <tr className='LoginForm-tr-special'></tr>
                </table>
            </center>
        </div>
        </div>
    );
}

export default RegistrationPage;