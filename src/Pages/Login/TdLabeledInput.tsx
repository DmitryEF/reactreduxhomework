export interface ITdLabeledInput {
    labelText: string,
    placeholderText: string,
    componentId: string,
    type: string
}

export default function TdLabeledInput(props: ITdLabeledInput) {
    const { labelText, placeholderText, componentId, type } = props;

    return (
        <>
            <td>
                <label>{labelText}</label>
            </td>
            <td>
                <input id={componentId} type={type} placeholder={placeholderText}></input>
            </td>
        </>
    );

}