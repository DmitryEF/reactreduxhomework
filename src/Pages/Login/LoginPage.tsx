import AuthorizationForm from "./AuthorizationForm";

const LoginPage = () => {
    return(
        <div>
            <AuthorizationForm />
        </div>
    );
}

export default LoginPage;