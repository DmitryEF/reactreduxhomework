import './AuthorizationForm.css';

import { useDispatch } from "react-redux";
import TdLabeledInput from './TdLabeledInput';
import { Button } from 'react-bootstrap';
import { setText } from '../../Store/InfoSlice';
import { useNavigate } from 'react-router-dom';

export default function AuthorizationForm() {  
    const navigate = useNavigate();

    const dispatch = useDispatch();

    const setInfoText = () =>{

        const e = document.getElementById("login") as HTMLInputElement;

        dispatch(setText(e.value));    

        navigate("/home");       
    };

    return (
        <div className="Auth-form-container">
            <center>
                <h1 className='LoginForm-h1'>Необходимо авторизоваться</h1>
                <br />
                <table className='LoginForm-table'>
                    <tr className='LoginForm-tr-special'></tr>
                    <tr>
                        <TdLabeledInput componentId="login" placeholderText='введите логин' labelText='Логин:' type='text'/>
                        <td className='LoginForm-td-special'></td>
                    </tr>
                    <tr>
                        <TdLabeledInput componentId="password" placeholderText='введите пароль' labelText='Пароль:' type='password'/>
                        <td className='LoginForm-td-special'></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <Button onClick={setInfoText}>Авторизоваться</Button>
                        </td>
                        <td className='LoginForm-td-special'></td>
                    </tr>
                    <tr className='LoginForm-tr-special'></tr>
                </table>
            </center>
        </div>
    );

}