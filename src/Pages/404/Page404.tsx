import BaseCard from "../../HoC/BaseCard";

const Page404 = () => {
    return(
        <div>
            <center>
                <BaseCard title="404" text="Page not found!"/>
            </center>
        </div>
    );
}

export default Page404;