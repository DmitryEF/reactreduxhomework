import { useSelector } from "react-redux";
import InformationCard from "../../HoC/InformationCard";

const HomePage = () => {
    const userName = useSelector((x : any) => x.info.text);

    return(
        <div>
            <center>
                <InformationCard title="Добро пожаловать" text={userName} loggedIn={userName !== ""}/>
            </center>
        </div>
    );
}

export default HomePage;