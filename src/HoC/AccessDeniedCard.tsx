import { Card, Spinner } from "react-bootstrap";

const AccessDeniedCard = () => {
    return(
        <div>
            <center>
                <Card style={{ marginTop: '100px', width: '50%' }}>
                    <Card.Body>
                        <Card.Title>Внимание! <Spinner animation="grow" variant="warning"/></Card.Title>                        
                        <Card.Text>
                            Вы не авторизовались! 
                        </Card.Text>
                    </Card.Body>
                </Card>                
            </center>
        </div>
    );
}

export default AccessDeniedCard;