import { Card } from "react-bootstrap";

const BaseCard = (props : {title: string, text: string}) => {
    return(
        <div>
            <center>
                <Card style={{ marginTop: '100px', width: '50%' }}>
                    <Card.Body>
                        <Card.Title><b>{props.title}</b></Card.Title>
                        <Card.Text>
                            {props.text}
                        </Card.Text>
                    </Card.Body>
                </Card>                
            </center>
        </div>
    );
}

export default BaseCard;