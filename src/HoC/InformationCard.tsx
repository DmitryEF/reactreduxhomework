import React from "react";
import AccessDeniedCard from "./AccessDeniedCard";
import BaseCard from "./BaseCard";

interface IWithInformationProps{
    loggedIn: boolean;
}

const withInformation = <P extends object>(Component: React.ComponentType<P>) =>
    class WithInformationCard extends React.Component<P & IWithInformationProps>{
        render(){
            const {loggedIn, ...props} = this.props;
            return loggedIn ? <Component {...props as P}/> : <AccessDeniedCard />;
        }
    }

const InformationCard = withInformation(BaseCard);

export default InformationCard;